# Redis Token Provider for NestJS 

Provides a named token `RedisToken`

Depends on both `@nestling/logger` and `@nestling/config`

The config service may provide:
```
redis: {
  port?: 6379,
  host?: 'localhost'
}
```
